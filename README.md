# terraform-s3-backend
Terraform configuration used to create s3 bucket. Using [here](https://gitlab.com/zektorum/terraform-k8s) as a Terraform backend.
